# Scryptids

Create a short numeric id from a user submitted public key.

The idea is simple: use the bytes from the public key as the password and salt for scrypt, turn the result into a 64 bit number and take the first 8 (or so) digits. See the example code for details.

# Why

Collisions are inevitable using short unique ids. Any id will need to be checked against a global list. If users can submit a public key and derived id two extra problems arise:

- Intentional id collisions, by generating keys and searching for an existing id
- Vanity ids (all threes) may be targeted and then traded or sold

The CPU and memory requirements of scrypt stop people from generating large numbers of public keys and evaluating the resulting short id.

The id space can be arbitrarily increased by taking more digits. Existing keys can be extended without causing collisions if: all existing ids are extended before checking new ids, or all new ids are checked against the previous, shorter length ids. The algorithm doesn't change in either case.

# Why Not

Denial of service, by submitting valid keys and random scrypt ids, forces the servers to run scrypt. Because of the higher resource demands rate limiting submissions is required. Using a server supplied nonce allows the server to control submission rate. If the nonce must be encrypted with the new key this can also ensure a valid public key pair is used.
