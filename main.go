package main

import (
	"crypto/elliptic"
	"crypto/rand"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"

	"golang.org/x/crypto/scrypt"
)

func main() {
	private, x, y, err := elliptic.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatal(err)
	}
	// our public key is the combination of bytes, without marshall or ASN.1
	var public []byte = append(x.Bytes(), y.Bytes()...)
	log.Println("private key: " + hex.EncodeToString(private))
	log.Println("public key: " + hex.EncodeToString(public))

	// use our public key in scrypt. N, r, p, len = 1<<20, 8, 4, 32
	// this needs one gigabyte of memory and runs 4 threads
	// on an intel 4770k it takes 10 seconds
	pass := public[0:16]
	salt := public[16:24]
	derivedKey, err := scrypt.Key(pass, salt, 1<<20, 8, 4, 32)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("scrypt key: " + hex.EncodeToString(derivedKey))

	// use the first 8 bytes as int64, then take the first 8 digits for our id
	i := binary.BigEndian.Uint64(derivedKey[0:8])
	id := fmt.Sprint(i)[0:8]
	log.Println("uint64: ", i)
	log.Println("our scrypt id: ", id)
}

